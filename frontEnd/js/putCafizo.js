function putCafizo(id) {
    window.location.href = '/frontEnd/editCafizo.html';
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonObj = JSON.parse(this.responseText);
            localStorage.setItem('cafizo', JSON.stringify(jsonObj))
            console.log(jsonObj);
        }
    };
    xmlhttp.open("GET", "http://127.0.0.1:8000/api/cafizo/" + id + "/", true);
    xmlhttp.send();
}

function editCafizo(){
    var cafizo = JSON.parse(localStorage.getItem('cafizo'));
    document.getElementById("cafizo_id").value = cafizo._id
    document.getElementById("cafizo_name").value = cafizo.name;
    document.getElementById("cafizo_nickname").value = cafizo.nickName;
    document.getElementById("age").value = cafizo.age;
    document.getElementById("cafizo_sex").value = cafizo.cafizoSex;
    document.getElementById("martialArt").value = cafizo.martialArt;
}

function registerC(){
    var cafizo = JSON.parse(localStorage.getItem('cafizo'));
    var id = cafizo.id;

    var _id = document.getElementById("cafizo_id").value;
    var cafizo_name = document.getElementById("cafizo_name").value;
    var cafizo_nickname = document.getElementById("cafizo_nickname").value;
    var age = document.getElementById("age").value;
    var cafizo_sex = document.getElementById("cafizo_sex").value;
    var martialArt = document.getElementById("martialArt").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        console.log(this.status);
        if (this.readyState == 4 && this.status == 204) {
            window.location.href='/frontEnd/showCafizo.html';
        }
    };
    xmlhttp.open("PUT", "http://127.0.0.1:8000/api/cafizo/" + id + "/", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify({
        "_id" : _id,
        "name" : cafizo_name,
        "nickName" : cafizo_nickname,
        "age" : age,
        "cafizoSex" : cafizo_sex,
        "martialArt" : martialArt
    }))
}