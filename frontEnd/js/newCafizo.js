function registerCafizo(){
    var _id = document.getElementById("cafizo_id").value;
    var cafizo_name = document.getElementById("cafizo_name").value;
    var cafizo_nickname = document.getElementById("cafizo_nickname").value;
    var age = document.getElementById("age").value;
    var cafizo_sex = document.getElementById("cafizo_sex").value
    var martialArt = document.getElementById("martialArt").value;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 201) {
            console.log(this.responseText);
            window.location.href='/frontEnd/showAlumno.html';
        }
    };
    xmlhttp.open("POST", "http://127.0.0.1:8000/api/cafizo/", true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify({
        
        "_id" : _id,
        "name" : cafizo_name,
        "nickName" : cafizo_nickname,
        "age" : age,
        "cafizoSex" : cafizo_sex,
        "martialArt" : martialArt
    }))
}
