function getAlumnos() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var jsonObj = JSON.parse(this.responseText);
            populateTable(jsonObj.objects);
        }
    };
    xmlhttp.open("GET", "http://127.0.0.1:8000/api/alumno/", true);
    xmlhttp.send();
}

function populateTable(jsonObj) {
    var col = [];
    for (let index = 0; index < jsonObj.length; index++) {
        for (var key in jsonObj[index]) {
            if (col.indexOf(key) === -1) {
                col.push(key);
            }
        }
    }
    var table = document.createElement("table");
    table.setAttribute('class', 'table table-hover table-dark');

    var thead = document.createElement("thead")

    for (var i = 0; i < col.length - 1; i++) {
        var th = document.createElement("th");
        th.innerHTML = col[i];
        th.setAttribute('class', 'scope');
        thead.appendChild(th);
    }

    var th = document.createElement("th");
    th.innerHTML = "Editar"
    th.setAttribute('class', 'scope');
    thead.appendChild(th);

    th = document.createElement("th");
    th.innerHTML = "Eliminar"
    th.setAttribute('class', 'scope');
    thead.appendChild(th);

    table.appendChild(thead)

    for (var i = 0; i < jsonObj.length; i++) {
        tr = table.insertRow(-1);
        var id;
        for (var j = 0; j < col.length - 1; j++) {
            var cell = tr.insertCell(-1);
            cell.innerHTML = jsonObj[i][col[j]];
            if (j == 3) {
                id = jsonObj[i][col[j]];
            }
        }
        var cell = tr.insertCell(-1);
        cell.innerHTML = '<button onclick="putAlumno('+ id +')">Editar</button>'
        cell = tr.insertCell(-1);
        cell.innerHTML = '<button onclick="deleteAlumno(' + id + ')">Eliminar</button>'
    }
    document.getElementById("alumnos").appendChild(table);
}

function newAlumno() {
    window.location.href = '/frontEnd/createAlumno.html';
}

function onSignIn(googleUser) 
{
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);

    //Algo para redireccionar aqui?
    window.location.href = '/frontEnd/showAlumno.html';
}

  function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
  }

  $(document).ready(function () {
    $('.carousel').carousel();
});